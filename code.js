/**
 * @param {boolean} enable
 * @param {HTMLElement} content
 * */
function contentTabFocus(enable, content) {
  const links = content.querySelectorAll("a");
  for (const e of links) {
    e.tabIndex = enable ? 0 : -1;
  }
}

/**
 * @param {HTMLElement} toggleBtn
 * @param {HTMLElement} content
 * */
function toggleHidden(toggleBtn, content) {
  if (content.classList.contains('hidden')) {
    toggleBtn.classList.add('hidden')
    contentTabFocus(false, content);
  } else {
    toggleBtn.classList.remove('hidden')
    contentTabFocus(true, content);
  }
}

const hideButtons = document.querySelectorAll('.hide-button')
hideButtons.forEach(btn => {
  const content = btn.parentElement.nextElementSibling;
  toggleHidden(btn, content);

  btn.addEventListener('click', () => {
    content.classList.toggle('hidden')
    toggleHidden(btn, content);
  })
})

fetch('bookmarks.html')
  .then(response => {
    if (response.ok)
      return response.text()
    else
      throw new Error(response.error)
  })
  .then(text => {
    const target = document.getElementById('bookmarks-target')
    target.innerHTML = text
    const bookmarks = target.querySelector('h1+dl').children
    target.replaceChildren(...bookmarks)
  })
  .catch(error => { console.error(error) })
  .finally(() => {
    const target = document.getElementById('bookmarks-target');
    const btn = target.parentElement.parentElement;
    contentTabFocus(!btn.classList.contains('hidden'), target);
    console.log(btn.classList.contains('hidden'));
  })
